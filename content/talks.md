---
title: "talks"
date: 2018-12-04T09:32:29-05:00
menu:
  main:
    title: talks
    weight:  3
---

- **Measuring Fuzzers**, _Empire Hacking, August 2019_ ([slides][fuzz])
- **Hacking During Work Hours**, _Avenger Con 2018_  ([slides][hdwh])
- **Cyber From First Principles**, Cyber Talks 2015 ([slides][cffp],
  [video][cffpv])

[cffp]:/Ragsdale-CyberTalks_2015-Cyber_From_First_Principles.pdf
[cffpv]:https://www.youtube.com/watch?v=qbwroHUuMzk
[hdwh]:/Ragsdale-AvengerCon_2018-Hacking_During_Work_Hours.pdf
[fuzz]:/Ragsdale-EmpireHacking-Measuring_Fuzzers.pdf
