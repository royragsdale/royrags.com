---
date: "2017-01-26T14:44:26-05:00"
title: "blog"
menu:
  main:
    title: blog
    weight: 2
---

> "Boys, do you know why I never became a writer? Because I never wrote the 
> first word."

Heck even these words are mostly cribbed from [Mr. John D. Rockefeller][roc]
himself.

[roc]:https://en.wikipedia.org/wiki/John_D._Rockefeller
