---
date: "2017-01-26T14:44:26-05:00"
title: "projects"
menu:
  main:
    title: projects
    weight:  3
---

- ~~[REDACTED]~~
- [myip][]
  - A simple "what is my IP?" service written in go.
  - [source][myipsrc]
- [configurator][conf]
  - A simple, highly opinionated, library to load a configuration.


[myip]:https://myip.royrags.com
[myipsrc]:https://gitlab.com/royragsdale/myip
[conf]:https://gitlab.com/royragsdale/configurator
